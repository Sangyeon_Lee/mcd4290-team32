let taxiList = [
	{"rego":"VOV-887","type":"Car","available":true},
	{"rego":"OZS-293","type":"Van","available":false},
	{"rego":"WRE-188","type":"SUV","available":true},
	{"rego":"FWZ-490","type":"Car","available":true},
	{"rego":"NYE-874","type":"SUV","available":true},
	{"rego":"TES-277","type":"Car","available":false},
	{"rego":"GSP-874","type":"SUV","available":false},
	{"rego":"UAH-328","type":"Minibus","available":true},
	{"rego":"RJQ-001","type":"SUV","available":false},
	{"rego":"AGD-793","type":"Minibus","available":false}
];


class Booking {



    constructor(){
        this.pickupMarker = null;
        this.destinationMarker = null;
        this.taxiType = null;
        this.pickup = null;
        this.destination = null;
        this.stops = [];
    }
}

let booking = new Booking()

//NAME: setTaxiType
//PURPOSE: set taxi type
//PARAMETER: taxiType
//RETURN VALUE: none
function setTaxiType(taxiType){

    booking.taxiType=taxiType
    document.getElementById('taxiType').innerHTML=booking.taxiType

}

//NAME: currentLocation
//PURPOSE: set current location
//PARAMETER: none
//RETURN VALUE: none
function currentLocation() {
    getUserCurrentLocationUsingGeolocation((lat, lon) => {
        setPickupLocation(lat, lon)
    });
}

//NAME: setPickupLocation
//PURPOSE: set pick up location
//PARAMETER: lat, lon
//RETURN VALUE: none
function setPickupLocation(lat, lon) {
    if (!booking.pickupMarker) {
        const marker = new mapboxgl.Marker({draggable: true})
        .setLngLat([lon, lat])
        .addTo(map);
        marker.on('dragend', () => {
            const lngLat = marker.getLngLat();
            sendWebServiceRequestForReverseGeocoding(lngLat.lat, lngLat.lng, 'pickupGeoCallback');
        });
        booking.pickupMarker = marker;
    } else {
        booking.pickupMarker.setLngLat([lon, lat])
    }

    map.flyTo({center:[lon, lat], speed: 5});
    booking.pickup = {lon: lon, lat: lat};
    sendWebServiceRequestForReverseGeocoding(lat, lon, 'pickupGeoCallback');
}

//NAME: setDestinationLocation
//PURPOSE: set destination location
//PARAMETER: lat, lon
//RETURN VALUE: none
function setDestinationLocation(lat, lon) {
    if (!booking.destinationMarker) {
        const marker = new mapboxgl.Marker({draggable: true, color: 'red'})
        .setLngLat([lon, lat])
        .addTo(map);
        marker.on('dragend', () => {
            const lngLat = marker.getLngLat();
            sendWebServiceRequestForReverseGeocoding(lngLat.lat, lngLat.lng, 'destinationGeoCallback');
        });
        booking.destinationMarker = marker;
    } else {
        booking.destinationMarker.setLngLat([lon, lat])
    }

    booking.destination = {lon: lon, lat: lat};
    sendWebServiceRequestForReverseGeocoding(lat, lon, 'destinationGeoCallback');
}

//NAME: addStop
//PURPOSE: add stop
//PARAMETER: lat, lon
//RETURN VALUE: none
function addStop(lat, lon) {
    booking.stops.push({lat: lat, lon: lon})
    const marker = new mapboxgl.Marker({draggable: true, color: 'yellow'})
    .setLngLat([lon, lat])
    .setPopup(new mapboxgl.Popup().setHTML(`<button onclick=''>Delete</button>`))
    .addTo(map);
    marker.on('dragend', () => {
//        const lngLat = marker.getLngLat();
//        sendWebServiceRequestForReverseGeocoding(lngLat.lat, lngLat.lng, 'destinationGeoCallback');
    });

}

//NAME: pickupGeoCallback
//PURPOSE: get pick up location
//PARAMETER: result
//RETURN VALUE: none
function pickupGeoCallback(result) {
    document.getElementById('pickup').innerHTML = result.results[0].formatted
}
//NAME: destinationFeoCallback
//PURPOSE: get destination location
//PARAMETER: result
//RETURN VALUE: none
function destinationGeoCallback(result) {
    document.getElementById('destination').innerHTML = result.results[0].formatted
}




