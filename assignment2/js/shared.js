"use strict";
// Keys for localStorage
const STUDENT_INDEX_KEY = "studentIndex";
const STUDENT_QUEUE_KEY = "queueIndex";
const APP_DATA_KEY = "consultationAppData";

class Student {
	constructor(fullName, studentId, problem) {
		this._fullName = fullName;
		this._studentId = studentId;
		this._problem = problem;
	}

	get fullName() {
		return this._fullName;
	}

	get studentId() {
		return this._studentId;
	}

	get problem() {
		return this._problem;
	}

	fromData(studentData) {
		student = JSON.parse(studentData);
		this._fullName = student.fullName;
		this._studentId = student.studentId;
		this._problem = student.problem;
	}
}

class Session {
	constructor() {
		this._startTime = new Date();
		this._queue = new Array();
	}

	get startTime() {
		return _startTime.toString();
	}

	get queue() {
		return _queue;
	}

	addSubQueue() {
		let a = new Array();
		this._queue.push(a);
	}

	addStudent(fullName, studentId, problem) {
		let shortestQueueIndex = 0;
		for (let i = 0; i < this._queue.length; i++) {
			if (this._queue[i].length < this._queue[shortestQueueIndex].length) {
				shortestQueueIndex = i;
			}
		}
		//alert(shortestQueueIndex);
		//alert(this._queue.length + " " + this._queue[0].length + " " + this._queue[1].length);

		this._queue[shortestQueueIndex].push(new Student(fullName, studentId, problem));
		updateLocalStorageData(APP_DATA_KEY, this);
	}

	removeStudent(studentIndex, queueIndex) {
		this._queue[queueIndex].splice(studentIndex, 1)
	}

	getStudent(studentIndex, queueIndex) {
		return this._queue[queueIndex][studentIndex];
	}

	fromData(queueData) {

		this._startTime = queueData._startTime;
		this._queue = queueData._queue;
	}
}

//NAME: checkLocalStorageDataExust
//PURPOSE: check local storage data exist
//PARAMETER: key
//RETURN VALUE: none
function checkLocalStorageDataExist(key) {
	if (localStorage.getItem(key)) {
		return true;
	}
	return false;
}

//NAME: updateLocationLocalStorageData
//PURPOSE: updata local storage data
//PARAMETER: key, data
//RETURN VALUE: none
function updateLocalStorageData(key, data) {
	let jsonData = JSON.stringify(data);
	localStorage.setItem(key, jsonData);
}

//NAME: getLocalStorageData
//PURPOSE: get local storage data
//PARAMETER: key
//RETURN VALUE: none
function getLocalStorageData(key) {
	let jsonData = localStorage.getItem(key);
	let data = jsonData;
	try {
		data = JSON.parse(jsonData);
	}
	catch (e) {
		console.error(e);
	}
	finally {
		return data;
	}
}


let consultSession = new Session();
if (checkLocalStorageDataExist(APP_DATA_KEY)) {
	// if LS data does exist
	let data = getLocalStorageData(APP_DATA_KEY);
	consultSession.fromData(data);
}
else {
	// if LS data doesn’t exists
	consultSession.addSubQueue();
	consultSession.addSubQueue();
	updateLocalStorageData(APP_DATA_KEY, consultSession);
}

//NAME: checkTime
//PURPOSE: check time
//PARAMETER: i
//RETURN VALUE: none
function checkTime(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}

//NAME: startTime
//PURPOSE: set start time
//PARAMETER: none
//RETURN VALUE: none
function startTime() {
	var today = new Date();
	var hour = today.getHours();
	var minite = today.getMinutes();
	var second = today.getSeconds();
	minite = checkTime(minite);
	second = checkTime(second);
	var timeField = document.getElementById('currentTime');
	if (timeField != null) {
		timeField.innerHTML = hour + ":" + minite + ":" + second;
	}
	var x = setTimeout(function () {
		startTime()
	}, 500);
}

window.onload = function () {
	startTime();
};