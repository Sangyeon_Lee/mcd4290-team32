"use strict";

//NAME: cancelBooking
//PURPOSE: cancel a specific booking
//PARAMETER: Booking
//RETURN VALUE: none
function cancelBooking(Booking){
    
    Booking.taxiType=null;
    Booking.pickupMarker=null;
    Booking.destinationMarker=null;
    Booking.pickup=null;
    Booking.destination=null;
    Booking.stops=null;

    document.getElementById('taxiType').innerHTML=Booking.taxiType;
    document.getElementById('pickup').innerHTML=Booking.pickup;
    document.getElementById('destination').innerHTML=Booking.destination;
    document.getElementById('stops').innerHTML=Booking.stops;

}