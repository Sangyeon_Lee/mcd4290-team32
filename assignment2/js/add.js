"use strict";
//NAME: addBooking()
//PURPOSE: add booking by typing
//PARAMETER: none
//RETURN VALUE: none
function addBooking() {
    document.getElementById("starting_msg").textContent = "";
    document.getElementById("destination_msg").textContent = "";
    document.getElementById("time_msg").textContent = "";

    var fullName = document.getElementById("starting").value;
    var studentId = document.getElementById("destination").value;
    var problem = document.getElementById("time").value;

    var inputValid = true;

    if (fullName == "") {
        document.getElementById("starting_msg").textContent = "Starting point cannot be empty";
        inputValid = inputValid && false;
    }
    else {
        document.getElementById("starting_msg").textContent = "";
        inputValid = inputValid && true;
    }

    if (problem == "") {
        document.getElementById("destination_msg").textContent = "Destination cannot be empty";
        inputValid = inputValid && false;
    }
    else {
        document.getElementById("destination_msg").textContent = "";
        inputValid = inputValid && true;
    }

    if (studentId == "") {
        document.getElementById("time_msg").textContent = "Time cannot be empty";
        inputValid = inputValid && false;
    }
    else {
        document.getElementById("time_msg").textContent = "";
        inputValid = inputValid && true;
    }

    if (inputValid) {
        consultSession.addStudent(fullName, studentId, problem);
        alert("success");
        window.location.replace("detailInformation.html");
    }
    else {
        return;
    }
}

//NAME: addAdditionalStops
//PURPOSE: add additional stops
//PARAMETER: none
//RETURN VALUE: none
function addAdditionalStops(){
    var fullName = document.getElementById("starting").value;
    let stop = prompt("Please input a additional stop.", "");
    for(let i = 0; i < consultSession._queue.length; i++){
        for(let j = 0; j < consultSession._queue[i].length; j++){
            if(consultSession._queue[i][j]._fullName == fullName){
                consultSession._queue[i][j]._problem += " ------ Additional Stop: " + stop;
                updateLocalStorageData(APP_DATA_KEY, consultSession)
                alert("success");
                window.location.replace("detailInformation.html");
                return;
            }
        }
    }
    alert("No starting point matched!");

}