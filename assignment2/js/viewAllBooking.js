"use strict";
function view(index, queueIndex) {
    alert("Starting point: " + consultSession._queue[queueIndex][index]._fullName + "\n" +
        "Destination: " + consultSession._queue[queueIndex][index]._studentId + "\n" +
        "Time: " + consultSession._queue[queueIndex][index]._problem);
}

function markDone(index, queueIndex) {
    
        consultSession.removeStudent(index, queueIndex);
        updateLocalStorageData(APP_DATA_KEY, consultSession);
    
    display(consultSession);
}

//NAME: display
//PURPOSE: display booking information
//PARAMETER: data
//RETURN VALUE: none
function display(data) {
    var content = document.getElementById("queueContent");
    content.innerHTML = '';
    for (let i = 0; i < data._queue.length; i++) {
        var ul = document.createElement("ul");
        ul.className = "mdl-list";


        for (let j = 0; j < data._queue[i].length; j++) {
            var li = document.createElement("li");
            li.className = "mdl-list__item mdl-list__item--two-line";

            var span = document.createElement("span");
            span.className = "mdl-list__item-primary-content";

            var ii = document.createElement("i");
            

            var nameSpan = document.createElement("span");
            nameSpan.textContent = "Starting point: " + data._queue[i][j]._fullName + "------Destination: " + data._queue[i][j]._studentId + "------Time: " + data._queue[i][j]._problem;

            span.appendChild(ii);
            span.appendChild(nameSpan);
            li.appendChild(span);

            var span1 = document.createElement("span");
            span1.className = "mdl-list__item-secondary-content";
            var a1 = document.createElement("a");
            a1.className = "mdl-list__item-secondary-action";
            a1.setAttribute("onclick", "view(" + j + "," + i + ")");
            var i1 = document.createElement("i");
            
            i1.textContent = "view detail";
			
            a1.appendChild(i1);
            span1.appendChild(a1);
            li.appendChild(span1);

			

            ul.appendChild(li);
        }

        content.appendChild(ul);
    }
}

display(consultSession);