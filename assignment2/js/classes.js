"use strict";


class Customer{
    constructor(){
        this._pickUpPoint = new String();
        this._destination = new String();
    }

    get pickUpPoint(){
        return this._pickUpPoint;
    }

    set pickUpPoint(startPoint){
        this._pickUpPoint = startPoint;
    }

    get destination(){
        return this._destination;
    }

    set destination(endPoint){
        this._destination = endPoint;
    }

    changePickUpPoint(newStartPoint){
        this._pickUpPoint = newStartPoint;
    }

    changeDestination(newEndPoint){
        this._destination = newEndPoint;
    }

    cancelBooking(){
        this._pickUpPoint = null;
        this._destination = null;
    }
}

class Booking{
    constructor(){
        this._stops = new String();
        this._distance = new Number();
        this._fare = new Number();
        this._taxiType = new String();
        this._time = Date();
    }

    get time(){
        return this._stops;
    }

    set time(){
        this._time = time;
    }

    get stops(){
        return this._stops;
    }

    set stops(){
        this._stops = stop1 + '; ' + stop2;
    }

    get distance(){
        return this._distance;
    }

    set distance(numDistance){
        this._distance = numDistance;
    }

    get fare(){
        return this._fare;
    }

    set fare(numFare){
        this._fare = numFare;
    }

    get taxiType(){
        return this._taxiType;
    }

    set taxiType(typeOfTaxi){
        this._taxiType = typeOfTaxi;
    }

    get bookingName(){
        return this._bookingName;
    }

    set bookingName(nameOfBooking){
        this._bookingName = nameOfBooking;
    }

    changeTaxiType(newTaxiType){
        this._taxiType = newTaxiType;
    }
    
    cancelBooking(){
        this._stops = null;
        this._distance = null;
        this._fare = null;
        this._taxiType = null;
        this._time = null;
        this._name = null;
    }
}