"use strict";
//NAME: view
//PURPOSE: the input of points
//PARAMETER: index, queneIndex
//RETURN VALUE: none
function view(index, queueIndex) {
    alert("Starting point: " + consultSession._queue[queueIndex][index]._fullName + "\n" +
        "Destination: " + consultSession._queue[queueIndex][index]._studentId + "\n" +
        "Time: " + consultSession._queue[queueIndex][index]._problem);
}



//NAME: display
//PURPOSE: display the detail information
//PARAMETER: data
//RETURN VALUE: none
function display(data) {
    var content = document.getElementById("queueContent");
    content.innerHTML = '';
    for (let i = 0; i < data._queue.length; i++) {
        var ul = document.createElement("ul");
        ul.className = "mdl-list";

        var queueHeader = document.createElement("h4");
        queueHeader.textContent = "Route " + (i + 1);

        ul.appendChild(queueHeader);

        for (let j = 0; j < data._queue[i].length; j++) {
            var li = document.createElement("li");
            li.className = "mdl-list__item mdl-list__item--three-line";

            var span = document.createElement("span");
            span.className = "mdl-list__item-primary-content";

            var ii = document.createElement("i");
            ii.className = "material-icons mdl-list__item-avatar";
            ii.textContent = "person";

            var nameSpan = document.createElement("span");
            nameSpan.textContent = "Starting point: " + data._queue[i][j]._fullName + "------Destination: " + data._queue[i][j]._studentId + "------Time: " + data._queue[i][j]._problem;
            //alert(data._queue[i][j]._fullName);

            span.appendChild(ii);
            span.appendChild(nameSpan);
            li.appendChild(span);

            var span1 = document.createElement("span");
            span1.className = "mdl-list__item-secondary-content";
            var a1 = document.createElement("a");
            a1.className = "mdl-list__item-secondary-action";
            a1.setAttribute("onclick", "view(" + j + "," + i + ")");

            var i1 = document.createElement("i");
            i1.className = "material-icons";
            i1.textContent = "info";

            a1.appendChild(i1);
            span1.appendChild(a1);
            li.appendChild(span1);


            var span2 = document.createElement("span");
            span2.className = "mdl-list__item-secondary-content";
            var a2 = document.createElement("a");
            a2.className = "mdl-list__item-secondary-action";
            a2.setAttribute("onclick", "markDone(" + j + "," + i + ")");

            var i2 = document.createElement("i");
            i2.className = "material-icons";
            i2.textContent = "done";

            a2.appendChild(i2);
            span2.appendChild(a2);
            li.appendChild(span2);

            ul.appendChild(li);
        }

        content.appendChild(ul);
    }
}

display(consultSession);