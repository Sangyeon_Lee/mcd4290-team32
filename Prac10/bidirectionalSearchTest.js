/* Write your function for implementing the Bi-directional Search algorithm here */
function
bidirectionalSearchTest(arr){
	
	for(let counter = 0; counter <= arr.length / 2; counter++) 
	{
		let obj = arr[arr.length/2 - counter];
		if (obj.emergency) {
			return obj.address;
		} 
		else
		{
			obj = arr[arr.length/2 + counter];
          	if (obj.emergency) {
          		return obj.address;
          	} 
        }

      }
      return null;
}